package model;

/**
 * @author Vaclav Rechtberger
 */
public enum Discount {
    NO_DISCOUNT,
    TWENTY_PERCENT_DISCOUNT,
    HUNDRED_CROWNS_DISCOUNT;
}
