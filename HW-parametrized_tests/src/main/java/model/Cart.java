package model;

/**
 * @author Vaclav Rechtberger
 */
public class Cart {
    private Float price;
    private Boolean containsAlcoholOrTobacco;

    public Cart(Float price, Boolean containsAlcoholOrTobacco) {
        if(price == null)
            throw new IllegalArgumentException("Price doesn't have to be null.");
        if(price <= 50)
            throw new IllegalArgumentException("Price has to be greater than 50 Czech crowns inclusive.");
        this.price = price;

        if(containsAlcoholOrTobacco == null)
            throw new IllegalArgumentException("Price doesn't have to be null.");
        this.containsAlcoholOrTobacco = containsAlcoholOrTobacco;
    }

    public Float getPrice() {
        return price;
    }

    public Boolean getContainsAlcoholOrTobacco() {
        return containsAlcoholOrTobacco;
    }
}
