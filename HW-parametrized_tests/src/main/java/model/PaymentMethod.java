package model;

/**
 * @author Vaclav Rechtberger
 */
public enum PaymentMethod {
    BANK_TRANSFER,
    PAY_PALL,
    CASH;
}
