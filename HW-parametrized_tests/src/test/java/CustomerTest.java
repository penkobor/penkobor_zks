import model.Address;
import model.Customer;
import model.State;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import sources.CustomerSource;

import static org.junit.jupiter.api.Assertions.*;


public class CustomerTest {

    @ParameterizedTest
    @ArgumentsSource(CustomerSource.class)
    void testInvalidCart(String name, State state, String addressInState, String phoneNumber, Integer age){
        Exception ex = assertThrows(RuntimeException.class, ()-> {
            Address address = new Address(state, addressInState);
            Customer customer = new Customer(name, address, phoneNumber, age);
        }, "exception was not thrown");
    }


}
