import model.Address;
import model.State;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class AddressTests {

    @ParameterizedTest
    @EnumSource(
            value = State.class,
            names = {"CZECH_REPUBLIC"},
            mode = EnumSource.Mode.EXCLUDE
    )
    void testIsAbroad(State state){
        Address address = new Address(state, " ");
        assertTrue(address.isAbroad());
    }

    private static Stream<Arguments> provideExceptionalParameters() {
        return Stream.of(
                Arguments.of(null, "some address"),
                Arguments.of(State.GERMANY, null),
                Arguments.of(State.GERMANY, "")
        );
    }

    @ParameterizedTest
    @MethodSource("provideExceptionalParameters")
    void shouldThrowException(State state, String adress){
        Exception ex = assertThrows(RuntimeException.class, ()-> {
            Address address = new Address(state, adress);
        }, "exception was not thrown");
    }

    @ParameterizedTest
    @EnumSource(
            value = State.class,
            names = {"CZECH_REPUBLIC"},
            mode = EnumSource.Mode.INCLUDE
    )
    void testIsNotAbroad(State state) {
        Address address = new Address(state, "some address" );
        assertFalse(address.isAbroad());
    }

}
