import model.Cart;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class CartTests {

    private static Stream<Arguments> provideExceptionalParameters() {
        return Stream.of(
                Arguments.of(null, null),
                Arguments.of(212.3F, null),
                Arguments.of(null, false),
                Arguments.of(3.44F, true),
                Arguments.of(30F, true),
                Arguments.of(30F, false),
                Arguments.of(12F, true),
                Arguments.of(12F, false)
        );
    }

    @ParameterizedTest
    @MethodSource("provideExceptionalParameters")
    void testInvalidCart(Float price, Boolean containsAlcoholOrTobacco){
        Exception ex = assertThrows(RuntimeException.class, ()-> {
            Cart cart = new Cart(price, containsAlcoholOrTobacco);
        }, "exception was not thrown");
    }

    @ParameterizedTest
    @CsvSource({"51F,true", "51F,false", "300F,true", "300F,false"})
    void testIfRightValuesAreReturned(Float price, Boolean containsAlcoholOrTobacco){
        Cart cart = new Cart(price, containsAlcoholOrTobacco);
        assertEquals(cart.getPrice(), price);
        assertEquals(cart.getContainsAlcoholOrTobacco(), containsAlcoholOrTobacco);
    }

    @ParameterizedTest
    @ValueSource(floats = {50.1F, 51F, 60.665F, 100.1F, Float.MAX_VALUE})
    void testIfPriceHigherThan50IsValid(Float price){
        Cart cart = new Cart(price, true);
        assertNotNull(cart);
    }
}
