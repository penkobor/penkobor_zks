import model.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import sources.OrderSource;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class OrderTest {

    @ParameterizedTest
    @ArgumentsSource(OrderSource.class)
    void testInvalidOrder(Discount discount, Customer customer, Cart cart, PaymentMethod paymentMethod){
        Exception ex = assertThrows(RuntimeException.class, ()-> {
            Order order = new Order(customer, cart, discount, paymentMethod);
        }, "exception was not thrown");
    }
}
