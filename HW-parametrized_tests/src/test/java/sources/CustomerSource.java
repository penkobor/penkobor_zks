package sources;

import model.Address;
import model.State;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

/**
 * @author Vaclav Rechtberger
 */
//(name, address, phoneNumber, age
public class CustomerSource implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(
                Arguments.of(null, State.CZECH_REPUBLIC, "", "3242", null),
                Arguments.of(null, State.CZECH_REPUBLIC, "a1", null, 5),
                Arguments.of(null, State.CZECH_REPUBLIC, "", null, 133),
                Arguments.of("", State.CZECH_REPUBLIC, "a1", "3242", null),
                Arguments.of("", State.CZECH_REPUBLIC, "", null, 145),
                Arguments.of("", State.CZECH_REPUBLIC, "a1", "3242", 12),
                Arguments.of("a", State.CZECH_REPUBLIC, "", null, null),
                Arguments.of("b", State.CZECH_REPUBLIC, "a1", "3242", 23),
                Arguments.of("c", State.CZECH_REPUBLIC, "", "3242", 153),
                Arguments.of("Arthur", State.CZECH_REPUBLIC, "", "3242", null),
                Arguments.of("Arthur", State.CZECH_REPUBLIC, "", null, 12),
                Arguments.of("Arthur", State.CZECH_REPUBLIC, "", "3242", 153),
                Arguments.of("", State.CZECH_REPUBLIC, "a1", "3242", 23),
                Arguments.of("", State.CZECH_REPUBLIC, "", null, 24),
                Arguments.of("", State.CZECH_REPUBLIC, "a1", "3242", 25),
                Arguments.of("", State.CZECH_REPUBLIC, "a1", "+420792268755", 3),
                Arguments.of("", State.CZECH_REPUBLIC, "", "+420792268755", 4),
                Arguments.of("", State.CZECH_REPUBLIC, "a1", "+420792268755", 125),
                Arguments.of("12345678 12345678 12345678 12345678", State.CZECH_REPUBLIC, "a1", null, null),
                Arguments.of("12345678 12345678 12345678 12345678", State.CZECH_REPUBLIC, "", "3242", 11),
                Arguments.of("12345678 12345678 12345678 12345678", State.CZECH_REPUBLIC, "a1", "3242", 121)
        );
    }
}

