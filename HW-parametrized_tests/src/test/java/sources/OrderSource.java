package sources;

import model.*;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class OrderSource implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        return Stream.of(
                Arguments.of(null, null, new Cart(100F, true), PaymentMethod.CASH),
                Arguments.of(null, new Customer("John", new Address(State.CZECH_REPUBLIC, "valid address"), "+420792233444", 19), null, null),
                Arguments.of(Discount.NO_DISCOUNT, null, null, PaymentMethod.CASH),
                Arguments.of(Discount.TWENTY_PERCENT_DISCOUNT, new Customer("John", new Address(State.CZECH_REPUBLIC, "valid address"), "+420792233444", 19), new Cart(100F, true), null),
                Arguments.of(Discount.TWENTY_PERCENT_DISCOUNT, null, new Cart(100F, true), PaymentMethod.CASH),
                Arguments.of(null,  new Customer("John", new Address(State.CZECH_REPUBLIC, "valid address"), "+420792233444", 19), new Cart(100F, true), PaymentMethod.CASH),
                Arguments.of(Discount.TWENTY_PERCENT_DISCOUNT,
                        new Customer("John",
                                new Address(State.CZECH_REPUBLIC, "valid address"), "+420792233444", 17),
                        new Cart(100F, true),
                        PaymentMethod.CASH),
                Arguments.of(Discount.TWENTY_PERCENT_DISCOUNT,
                        new Customer("John",
                                new Address(State.GERMANY, "valid address"), "+420792233444", 19),
                        new Cart(100F, true),
                        PaymentMethod.CASH),
                Arguments.of(Discount.HUNDRED_CROWNS_DISCOUNT,
                        new Customer("John",
                                new Address(State.CZECH_REPUBLIC, "valid address"), "+420792233444", 19),
                        new Cart(99F, true),
                        PaymentMethod.CASH),
                Arguments.of(Discount.TWENTY_PERCENT_DISCOUNT,
                        new Customer("John",
                                new Address(State.CZECH_REPUBLIC, "valid address"), "+420792233444", 19),
                        new Cart(100F, true),
                        PaymentMethod.CASH)
        );
    }
}
