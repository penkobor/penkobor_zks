import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import sources.AnotherSource;

import java.util.EnumSet;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Vaclav Rechtberger
 */
public class ExampleTest {
    @Test
    public void test1(){
        Exception ex = assertThrows(RuntimeException.class, ()->{
            throw new RuntimeException("expected message");
        }, "Exception was not trown");
        assertEquals(ex.getMessage(), "expected message", "Exception does not have expected message");
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "  "})
    void test2(String input) {
    }

    @ParameterizedTest
    @NullSource
    void test3(String input) {
    }

    @ParameterizedTest
    @EmptySource
    void test4(String input) {
    }

    @ParameterizedTest
    @NullAndEmptySource
    void test5(String input) {
    }

    enum MyEnum{
        VAL_1,
        VAL_2,
        VAL_3,
        VAL_4,
        VAL_11;
    }

    @ParameterizedTest
    @EnumSource(MyEnum.class) // passing all 12 months
    void test6(MyEnum myEnum) {
        System.out.println(myEnum);
    }

    @ParameterizedTest
    @EnumSource(value = MyEnum.class, names = {"VAL_1", "VAL_2"})
    void test7(MyEnum myEnum) {
        System.out.println(myEnum);
    }

    @ParameterizedTest
    @EnumSource(
            value = MyEnum.class,
            names = {"VAL_1", "VAL_2"},
            mode = EnumSource.Mode.EXCLUDE)
    void test8(MyEnum myEnum) {
        System.out.println(myEnum);
    }

    @ParameterizedTest
    @EnumSource(value = MyEnum.class, names = "VAL_1.*", mode = EnumSource.Mode.MATCH_ANY)
    void test9(MyEnum myEnum) {
        EnumSet<MyEnum> myEnums =
                EnumSet.of(MyEnum.VAL_1, MyEnum.VAL_11);
        assertTrue(myEnums.contains(myEnum));
    }

    @ParameterizedTest
    @CsvSource({"test,TEST", "tEst,TEST", "Java,JAVA"})
    void test10(String input, String expected) {

    }

    @ParameterizedTest
    @CsvSource(value = {"test:test", "tEst:test", "Java:java"}, delimiter = ':')
    void test11(String input, String expected) {

    }

    /**
     * we can return any other collection-like interfaces like List
     * @return
     */
    private static Stream<Arguments> providerMethod() {
        return Stream.of(
                Arguments.of(null, true),
                Arguments.of("", true),
                Arguments.of("  ", true),
                Arguments.of("something", false)
        );
    }

    @ParameterizedTest
    @MethodSource("providerMethod")
    void test12(String input, boolean expected) {
    }

    @ParameterizedTest
    @MethodSource // no method specified?
    void test13(String input) {
    }


    private static Stream<String> test13() { // does not matter, JUnit will search for method with same name
        return Stream.of(null, "", "  ");
    }


    @ParameterizedTest
    @MethodSource("sources.OtherSource#someSource")
    void test14(String input) {
    }

    @ParameterizedTest
    @ArgumentsSource(AnotherSource.class)
    void test15(String input) {
    }

}
